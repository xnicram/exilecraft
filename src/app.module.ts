import { NgModule, ApplicationRef } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { App } from "./app.component";
import { BuildSummary } from "./buildsummary/buildsummary.component";
import { NodeDetails } from "./nodedetails/nodedetails.component";
import { TreeView } from "./treeview/treeview.component";
import { ActionBar } from "./actionbar/actionbar.component";


@NgModule({
	bootstrap: [App],
	declarations: [App, BuildSummary, NodeDetails, TreeView, ActionBar],
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule
	],
	providers: []
})
export class AppModule
{
	constructor(public appRef: ApplicationRef) {}
}